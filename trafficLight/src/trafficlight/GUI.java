
package trafficlight;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {
    
    private JPanel panel = new JPanel();
    
    @Override
    public void paint(Graphics g){                    
 
    
    g.fillArc(180, 50, 70, 30, 0, 180); // крышка
    
    g.fillRoundRect(170, 70, 90, 315, 20, 15); //тело 
    
    g.fillRoundRect(193, 387, 45, 65, 20, 15); //нога
    
    //белые круги
    Color whiteColor = new Color(255, 255, 255);
    g.setColor(whiteColor);
    g.fillOval(175, 80, 80, 80);
    g.fillOval(175, 180, 80, 80);
    g.fillOval(175, 280, 80, 80);
    
    //черные круги
    Color blackColor = new Color(0, 0, 0);
    g.setColor(blackColor);
    g.fillOval(175, 90, 80, 80);
    g.fillOval(175, 190, 80, 80);
    g.fillOval(175, 290, 80, 80);
    
    //красный
    Color redColor = new Color(255, 0, 0);
    g.setColor(redColor);
    g.fillOval(180, 100, 70, 70);
    
    //желтый
    Color yellowColor = new Color(255, 255, 0);
    g.setColor(yellowColor);
    g.fillOval(180, 200, 70, 70);
    
    //зелёный
    Color greenColor = new Color(0, 255, 0);
    g.setColor(greenColor);
    g.fillOval(180, 300, 70, 70);
    
    //левый верхний козырек
    g.setColor(blackColor);
    g.fillRoundRect(115, 90, 50, 10, 10, 10);
    g.fillRoundRect(155, 90, 10, 50, 10, 10);
    int xp[]= {116,159,159};
    int yp[]= {97, 95, 140};
    g.fillPolygon(xp,yp,3);
    
    //левый средний козырек
    g.fillRoundRect(115, 190, 50, 10, 10, 10);
    g.fillRoundRect(155, 190, 10, 50, 10, 10);
    int yp2[] = {197, 195, 240};
    g.fillPolygon(xp,yp2,3);
    
    //левый нижний козырек
    g.fillRoundRect(115, 290, 50, 10, 10, 10);
    g.fillRoundRect(155, 290, 10, 50, 10, 10);
    int yp3[] = {297, 295, 340};
    g.fillPolygon(xp,yp3,3);
    
    //правый верхний козырек
    g.fillRoundRect(265, 90, 50, 10, 10, 10);
    g.fillRoundRect(265, 90, 10, 50, 10, 10);
    int xp1[]= {270,316,272};
    g.fillPolygon(xp1,yp,3);
    
    //правый средний козырек
    g.fillRoundRect(265, 190, 50, 10, 10, 10);
    g.fillRoundRect(265, 190, 10, 50, 10, 10);
    g.fillPolygon(xp1,yp2,3);
    
    //правый нижний козырек
    g.fillRoundRect(265, 290, 50, 10, 10, 10);
    g.fillRoundRect(265, 290, 10, 50, 10, 10);
    g.fillPolygon(xp1,yp3,3);
    
    
}  
    
    
    public GUI(){
        super("MyFirstJavaGUI");
        this.setBounds(100, 100, 500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
    